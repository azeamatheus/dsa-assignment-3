import ballerina/http;
import ballerina/log;
import ballerina/io;


type CourseOutline record {
    int Id;
    json courseInformation;
    json LeactureInformation;
    json LearningOutcomes;
    json CourseContent;
    boolean IsSignedByLeavture;
    boolean IsApprovedByHoD;
};

type LeactureInformation record {|
    string Name;
    string Emai;
    string OfficePhone;
    string OfficeLocation;
    string OfficeHours;
    string ConsultationHours;
|};



json body = {
"CourseOutlineInformation": 
{
"Code": "DSA621S",
        "Title": "Ditributed systems and Applications",
        "Programme": "07BACS",
        "Department": "Computer science",
        "ContactHours": "120 Hours",
        "CourseFormat": "45 hours face to face , face contact with the lecturer,42 hours directed self learning and self directed learning",
        "EffectiveDate": "17 August 2021",
        "Prerequisiites": "Programming 2",
        "CourseDescription": "The aim of this course is to introduce the students to the principles andconcepts of distributed systems,protocols and interfaces",
        "CourseEquivalence": "Object-oriented Programming (OOP521S)",
        "CourseDeliveryMethods": "The delivey of the course ids theory and practical. The practical will be based on a student-centered approach.it will also involve a group of students carrying out a  group project"

},
"LecturerInformation":[{

   " Lecturer’s name": "José G. Quenum",
"Email": "jquenum@nust.na",
"Office Phone":" 207 2235",
"Office Location":"Lower Campus, Science & Technology Bld.",
"Office hours": "08:00 – 16:30",
"Consultation hours": "Wednesdays (10:30 – 12:30)"

}

],
"LearningOutcomes":[
   " Analyse the principles and mechanisms used in Distributed systems and appliications",
   " Demonstrate knowledge of the current application and their use"
],
         "Coursecontent":["Distributed Systems and their characteristics",
                         "Applications: Service‐oriented Computing",
                         " Communication (Inter‐process Communication,Remote Invocation,Indirect Communication, API design and Implementation)",
                         "More Applications( Distributed Data Stores, Security & Fault Tolerance)",
                         "Distributed Algorithms( Logical Time & Global State,Coordination & Consensus,Transactions and concurrency model)"
],
                         "IsSignedByLecturer": false,
                         "IsApprovedByHoD" :false
}
;

service /courseOutline/Leacture on new http:Listener(9096) {   //courseOutline service listens on the 9096 port

    resource function get viewCourseOutlines(http:Caller caller, int Id) returns error? {
        http:Response response = new ();                                     // function to VIEW CourseOutline

        if (Id == 0) {
            response.setJsonPayload({"Message": "Invalid payload - lectureName"});
            var result = caller->respond(response);
            if (result is error) {
                log:printError("Error sending response", result);    //if id is incorect then error sending response
            }
        } else {
            stream<CourseOutline, error?> resultStream =     

            json[] courseOutlines = [];         //display courseOutline in JSON format

            error? e = resultStream.forEach(function(CourseOutline result) {
                courseOutlines.push(result.toJson());
                io:println("\n");
            });

            response.setJsonPayload(courseOutlines);

            var result = caller->respond(response);
            if (result is error) {
                log:printError("Error sending response", result);
            }

        }
    }

resource function post createCourseOutline(http:Caller caller, http:Request request) returns error? { //function to create/generate courseOutline
        http:Response response = new ();
        
        var courseOutlineReq = request.getJsonPayload();
        if (courseOutlineReq is error) {
            response.setJsonPayload({"Message": "Invalid payload - Not a valid JSON payload"});
            var result = caller->respond(response);
            if (result is error) {
                log:printError("Error sending response", result);
            }

        } else {

            json|error courseInformation = courseOutlineReq.courseInformation;
            json|error LeactureInformation = courseOutlineReq.LeactureInformation;
            json|error LearningOutcomes = courseOutlineReq.LearningOutcomes;
            json|error CourseContent = courseOutlineReq.CourseContent;
            json|error IsSignedByLeacture = courseOutlineReq.IsSignedByLeacture;
            json|error IsApprovedByHoD = courseOutlineReq.IsApprovedByHoD;

            if (courseInformation is error || LeactureInformation is error || LearningOutcomes is error || CourseContent is error || IsSignedByLeacture is error || IsApprovedByHoD is error) {

                response.setJsonPayload({"Message": "wrong Request: Invalid payload"});
                var responseResult = caller->respond(response);
                if (responseResult is error) {
                    log:printError("Error sending response", responseResult);
                }

            } else {
                _ = check json ->execute(`INSERT INTO CourseOutline(Id,courseInformation,LeactureInformation,LearningOutcomes,CourseContent,IsSignedByLeacture,IsApprovedByHoD)
                VALUES(0,${courseInformation.toString()},${LeactureInformation.toString()},${LearningOutcomes.toString()},${CourseContent.toString()},${<boolean>IsSignedByLeacture},${<boolean>IsApprovedByHoD})`);


                json payload = {status: "Course Outline successfully created"};
                response.setJsonPayload(payload);
                var result = caller->respond(response);
                if (result is error) {
                    log:printError("Error sending response", result);
                }
            }
        }
    }

    resource function put signCourseOutline(http:Caller caller, int Id) returns error? {
        http:Response response = new ();
        

        if (Id == 0) {
            response.setJsonPayload({
                "Message": "Invalid payload - Id or approave code"});
            var result = caller->respond(response);
            if (result is error) {
                log:printError("Error sending response", result);
            }
        } else {
            _ = check json->execute(`UPDATE CourseOutline SET IsSignedByLeacture=${true}     WHERE Id =${Id}    ;`);

            check json.close();

            json payload = {status: "Course Outline approaved by HoD"};
            response.setJsonPayload(payload);
            var result = caller->respond(response);
            if (result is error) {
                log:printError("Error sending response", result);
            }
        }
    }
}
